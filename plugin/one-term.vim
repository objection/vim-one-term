if !has ('terminal')
	echoerr "You don't need this: your Vim doesn't have the terminal feature"
	finish
elseif exists ('g:one_term_loaded')
    finish
endif

let g:one_term_loaded = 1

func! s:one_term (mods, split = 'yes')
	" I'm using switchbuf = useopen to have you jump to one-terms,
	" so save user's setting.
	let saved_switchbuf = &l:switchbuf
	setl switchbuf=usetab
	" If you're running the function for the first time or you've deleted
	" the terminal.
	if !exists ('g:one_term_term_set') || !bufexists (g:one_term_term_set)
		" Get a list of currently loaded terminals
		" Note: I've done buflisted. There's other options I could have chosen
		" that might have been better.
		" Note: I've realised  now there's a term_list() functino that does this.
		" But I might later do some other stuff in this loop, like check the terminal
		" status.
		let bufinfos = getbufinfo ({'buflisted': 1})
		let terms = []
		for bufinfo in bufinfos
			let type = getbufvar (bufinfo.bufnr, '&buftype')
			if type ==# 'terminal'
				call add (terms, bufinfo.bufnr)
			endif
		endfor
		" If you've got no terms at all, open one and add its bufnr to
		" g:one_term_term_set to reference next time through the function.
		if len (terms) == 0
			if a:split ==# '++curwin'
				exe a:mods 'term ++curwin'
			else
				exe a:mods 'term'
			endif
			call add (terms, bufnr ())
		" If you've got more than one, just choose the first. 
		" I've looked to see if there's a way to tell if the terminal job is
		" finished and can't find it. If there is, it'd be good to only
		" choose proper terminals. 
		elseif len (terms) > 1
			echom 'More than one term: choosing the first'
		endif
		let g:one_term_term_set = terms[0]
	else
		" You've already got a g:one_term_term_set, so just open it.
		if a:split ==# '++curwin'
			" This bit here means if you have a one_term open and you do ++curwin, you
			" won't get a new view on the terminal. You'll jump to the open on. This seems
			" like what you want, to me. It does mean that ++curwin will only have an
			" effect when you're doing Term for the first time.
			if g:one_term_term_set
				exe a:mods 'sb' g:one_term_term_set
			else
				exe a:mods 'b' g:one_term_term_set
			endif
		else
			exe a:mods 'sb' g:one_term_term_set
		endif
	endif
	exe 'setl switchbuf=' . saved_switchbuf
endfunc

" See One_term
" first arg: mods (see h <mods>).
" second arg: make this ++curwin or nothing. Similar to how :term works.
command! -nargs=? Term
			\ call s:one_term (<q-mods>, <q-args>)

" Gets rid of g:one_term_term_set.
command! Noterm
			\ if g:one_term_term_set|
			\ 	kX|
			\ 	exe bufwinnr (g:one_term_term_set) . 'windo q'|
			\ 	exe 'normal! `X' |
			\ endif
