# One Term

Dirt-simple script to make sure you only have one terminal at a time.

It works like this.

* Do :Term, and get a terminal.
* Do :Term again from wherever to jump to the terminal.
* Call :Noterm if you like to make it go away. You can get rid of it
  in the normal way if you prefer.
* Call :Term again to make it pop up again.

It's similar to a plugin called Nuake, except that makes a terminal at
the bottom of the screen. This just makes a normal Vim terminal that
you can manipulate in the usual ways.

eg, you can do `vert Term` to make the split vertical instead of
horizonal (terminals in Vim, like many Vim buffer-making commands,
split by default). You can also do `Term ++curwin` to have the window
take over the current window. But only the first time! After you've
got a "one term", you'll always jump to is instead of making a new
view on it.

You can only have _one term_. I can well imagine people wanting more,
perhaps one per tab page. But this doesn't do that. If anyone wants to
hack that in -- something like `Termtab` and `Notermtab` -- gimme a
pull request.

I've not bothered with bindings. I'll probably put in a "toggle" one.

## Vague todos

* Something like Termmove. You might want to move the terminal. You
  can do that with Noterm and Term, of course.
* Optional one-terms per tab page. `Termtab`, something like that.
